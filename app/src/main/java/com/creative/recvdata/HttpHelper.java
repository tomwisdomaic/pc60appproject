package com.creative.recvdata;
import android.os.StrictMode;

import com.cls.comClass;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

public class HttpHelper extends comClass {

    private String Url;
    private String Data;
    public String ResponseContent;

    public Map<String, String> getParmValues() {
        return ParmValues;
    }

    public void setParmValues(Map<String, String> parmValues) {
        ParmValues = parmValues;
    }

    private Map<String, String> ParmValues;
    public boolean isPosting() {
        return Posting;
    }

    public void setPosting(boolean posting) {
        Posting = posting;
    }

    public boolean isSuccessPost() {
        return SuccessPost;
    }

    public void setSuccessPost(boolean successPost) {
        SuccessPost = successPost;
    }

    private boolean Posting=false;
    private boolean SuccessPost=false;

    public String getCaptionData() {
        return CaptionData;
    }

    public void setCaptionData(String captionData) {
        CaptionData = captionData;
    }
    private String CaptionData;

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }
    public HttpHelper(String url)
    {
        Url=url;
    }
    public HttpHelper(String url,String data,String captionData) {
        Url=url;
        Data = data;
        CaptionData = captionData;
    }
    public void PostAll()
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        new Thread(PostData).run();
    }
    Runnable PostData = new Runnable(){
        @Override
        public void run() {
            // TODO Auto-generated method stub
            Post(Url,Data,CaptionData);
        }
    };
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }
    protected String Post(String url,String data,String captionData) {
        setPosting(true);
        InputStream inputStream = null;
        try {
                // 1. create HttpClient
                DefaultHttpClient httpclient = new DefaultHttpClient();
                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(url);
                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                if(ParmValues==null) {
                    jsonObject.accumulate("value", (data==null?"":data));
                    if (captionData != null && captionData != "") {
                        jsonObject.accumulate("caption", captionData);
                    }
                }else
                {
                    for(String key : ParmValues.keySet())
                    {
                        jsonObject.accumulate(key, ParmValues.get(key));
                    }
                }
                // 4. convert JSONObject to JSON to String
                String json = jsonObject.toString();
                // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);
                List postParameters = new ArrayList<NameValuePair>();
                postParameters.add(new BasicNameValuePair("dataset", json));
                // 5. set json to StringEntity
                //StringEntity se = new StringEntity(json);
                httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
                // 6. set httpPost Entity
                //httpPost.setEntity(se);
                // 7. Set some headers to inform server about the type of the content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);
                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                // 10. convert inputstream to string
                if (inputStream != null)
                    ResponseContent = convertInputStreamToString(inputStream);
                else
                    ResponseContent = null;

        } catch (Exception e) {
            error = e;
            setSuccessPost(false);
        }finally {

            setPosting(false);
        }
        // 11. return result
        return ResponseContent;
    }

    public String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
        result += line;
        inputStream.close();
        return result;

    }
}
