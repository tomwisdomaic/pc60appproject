package com.creative.recvdata;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;
import android.widget.Toast;

import com.creative.hrv.comActivity;

public class UsbService extends comActivity {
    private static final int LEVITICUS_VENDOR_ID=0x200; //device id
    private static final int LONG_DELAY = 3500; // 3.5 seconds
    private static final int SHORT_DELAY = 2000; // 2 seconds
    private BroadcastReceiver mUsbReceiver;
    @SuppressLint("WrongConstant")
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        String message = "action:"+action+" device:"+device.getProductId()+" Vendor:"+device.getVendorId();

        Toast.makeText(context, message, LONG_DELAY).show();

        if(device.getVendorId()==LEVITICUS_VENDOR_ID)
        {
            if(action.equals(UsbManager.ACTION_USB_DEVICE_ATTACHED))
            {
                handleDeviceAttached(device);
            }
            if(action.equals(UsbManager.ACTION_USB_DEVICE_DETACHED))
            {
                handleDeviceDettached(device);
            }
        }
    }
    private void handleDeviceAttached(UsbDevice device)
    {
        Log.v(device.getDeviceName()+"Attached","USBDevice");
    }
    private void handleDeviceDettached(UsbDevice device)
    {
        Log.v(device.getDeviceName()+"Dettached","USBDevice");
    }
    private void registerUsbReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        registerReceiver(mUsbReceiver, filter);
        }
}
