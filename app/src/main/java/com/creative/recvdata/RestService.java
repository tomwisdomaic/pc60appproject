package com.creative.recvdata;
import android.content.Context;

import com.cls.comClass;
import com.creative.libdemo.R;
import com.creative.model.OxyRawData;
import com.squareup.okhttp.OkHttpClient;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class RestService extends comClass {

    public boolean isPostReturn() {
        return PostReturn;
    }

    public void setPostReturn(boolean postReturn) {
        PostReturn = postReturn;
    }

    private boolean PostReturn;
    public boolean isPosting() {
        return Posting;
    }

    public void setPosting(boolean posting) {
        Posting = posting;
    }

    private boolean Posting;

    public String getBaseUrl() {
        return BaseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        BaseUrl = baseUrl;
    }
    private String BaseUrl;
    public RestService(Context context)
    {
        BaseUrl = context.getResources().getString(R.string.app_api_resturl);
    }
    public RestService(String baseUrl)
    {
        BaseUrl = baseUrl;
    }
    public void Get(Integer Id,String oxyRawData) {
        RestAdapter.Builder builder =
                new RestAdapter.Builder()
                        .setEndpoint(BaseUrl)
                        .setClient(
                                new OkClient(new OkHttpClient()));
        try {
            RestAdapter adapter = builder.build();
            RestInter client = adapter.create(RestInter.class);
            // Fetch a list of the Github repositories.
            client.Get(Id, new Callback<String>() {
                @Override
                public void success(String result, Response response) {
                    // The network call was a success and we got a response
                    // TODO: use the repository list and display it
                    String res = response.getBody().toString();
                }

                @Override
                public void failure(RetrofitError error) {
                    // the network call was a failure or the server send an error
                    // TODO: handle error
                    String message = error.getMessage();
                }
            });
        }catch (Exception ex)
        {
            error = ex;

        }
    }

    public void Post(Integer Id,String parmValue) {
        setPosting(true);
        setPostReturn(false);
        RestAdapter.Builder builder =
                new RestAdapter.Builder()
                        .setEndpoint(BaseUrl)
                        .setClient(
                                new OkClient(new OkHttpClient()));
        RestAdapter adapter = builder.build();
        RestInter client = adapter.create(RestInter.class);
        boolean lbRet = false;
        try {
            // Fetch a list of the Github repositories.
            client.Post(Id,parmValue, new Callback<String>() {
                @Override
                public void success(String result, Response response) {
                    // The network call was a success and we got a response
                    // TODO: use the repository list and display it
                    String resultStr = response.getBody().toString();
                    setPostReturn(decodeReturnResult(resultStr));
                    setPosting(false);
                }
                @Override
                public void failure(RetrofitError error) {
                    // the network call was a failure or the server send an error
                    // TODO: handle error
                    String message = error.getMessage();
                    setPosting(false);
                }
            });
        }catch (Exception ex)
        {
            error = ex;
    setPosting(false);
}
    }
    private boolean decodeReturnResult(String returnResult)
    {


        returnResult=returnResult.toLowerCase();
        if (returnResult == "ok" || returnResult == "success" || returnResult=="yes" || returnResult== "true")
            return true;
        else
            return false;
    }

}
