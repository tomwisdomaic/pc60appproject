package com.creative.recvdata;


import java.util.ArrayList;
import java.util.List;

public class WaveData
{
    public int getTag() {
        return Tag;
    }

    public void setTag(int tag) {
        this.Tag = tag;
    }
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }
    public List<Integer> getData() {
        return Data;
    }

    public void setData(List<Integer> data) {
        this.Data = data;
    }

    private int Tag;
    private String Status;
    private List<Integer> Data = new ArrayList<Integer>();

}
