package com.creative.recvdata;
import android.app.Activity;
import android.app.Application;

import java.util.ArrayList;
import java.util.List;

public class AgentApplication {

    public static Application getApplication() {
        return application;
    }

    public static void setApplication(Application application) {
        AgentApplication.application = application;
    }

    private static Application application;

    private List<Activity> activities = new ArrayList<Activity>();
    public void AddActivity(Activity activity) {
        activities.add(activity);
    }
    public void Exit()
    {
        onTerminate();
    }

    public void onTerminate() {

        for (Activity activity : activities) {
            activity.finish();
        }
        application.onTerminate();
        onDestroy();
        System.exit(0);
    }
    private void onDestroy()
    {
        // dispose resources
    }
}