package com.creative.recvdata;
import com.creative.model.OxyRawData;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

public interface RestInter {
    //i.e. http://localhost/api/institute/Students/1
    //PUT student record and post content in HTTP request BODY
    @GET("/values/{id}")
    void Get(@Path("id") Integer Id, Callback<String> callback);

    /*@DELETE("/values/{id}")
    void Delete(@Path("id") Integer Id, Callback<String> callback);*/

    @POST("/values/{id}")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    void Post(@Path("id") Integer Id, @Body String value, Callback<String> callback);

    /*@PUT("/values/{id}")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    void Put(@Path("id") Integer Id, @Body String value, Callback<String> callback);*/
}
