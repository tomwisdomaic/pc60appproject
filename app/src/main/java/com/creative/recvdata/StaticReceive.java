package com.creative.recvdata;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.creative.FingerOximeter.FingerOximeter;
import com.creative.FingerOximeter.IFingerOximeterCallBack;
import com.creative.base.BLUReader;
import com.creative.base.BLUSender;
import com.creative.base.BaseDate.Wave;

public class StaticReceive {

	/**
	 *
	 * PC-60NW serails protocal analyse
	 */
	private static FingerOximeter finger;

	/**
	 * send message to upper layers
	 */
	private static Handler mHandler;

	@SuppressWarnings("unused")
	private static Context mContext;

	/**
	 * begin receive wave data
	 * 
	 * @param bluName
	 */
	public static void startReceive(Context context, String bluName,
			BLUReader iReader, BLUSender iSender, Handler _handler) {
		if (bluName != null && !bluName.equals("")) {
			start = true;
			mHandler = _handler;
			mContext = context;
			if (bluName.equals("PC-60NW-1") || bluName.equals("PC-60NW")
					|| bluName.equals("POD")) {
				finger = new FingerOximeter(iReader, iSender,
						new FingerCallBack());
				finger.Start();
				finger.SetWaveAction(true);// set wave enable
				finger.QueryDeviceVer();// query device version
			}
		}
	}

	/**
	 * stop receive data
	 */
	public static void StopReceive() {
		start = false;
		if (finger != null) {
			finger.Stop();
			finger = null;
		}
		HWMajor = HWMinor = SWMajor = SWMinor = ALMajor = ALMinor = 0;
	}

	/**
	 * pause to receive data
	 */
	public static void Pause() {
		pause = true;
		if (finger != null) {
			finger.Pause();
		}
	}

	/**
	 * recover to receive data
	 */
	public static void Continue() {
		pause = false;
		if (finger != null) {
			finger.Continue();
		}
	}

	public static boolean pause = false;

	public static boolean start = false;

	public static void setmHandler(Handler mHandler) {
		StaticReceive.mHandler = mHandler;
	}

	/**
	 * device message of SPO2 parameter
	 */
	public static final int MSG_DATA_SPO2_PARA = 0x203;

	/**
	 * device message of break connection
	 */
	public static final int MSG_DATA_DISCON = 0x208;

	/**
	 * device message of pulse tag
	 */
	public static final int MSG_DATA_PULSE = 0x20f;

	/**
	 * Ouput Wave Data. Modified by Tom 2018-06-18
	 */
	public static final  int MSG_DATA_WAVE = 0x200;

	/**
	 * Save wavedata for drawing the rectangle, list for drawing spo2 rect
	 */
	public static List<Wave> SPOWAVE = new ArrayList<Wave>();

	/**
	 * device version
	 */
	public static int HWMajor, HWMinor, SWMajor, SWMinor, ALMajor, ALMinor;

	/**
	 * save wave data to array
	 */
	public static List<Wave> DRAWDATA = new ArrayList<Wave>();


	private static class FingerCallBack implements IFingerOximeterCallBack {

		@Override
		public void OnConnectLose() {
			mHandler.sendEmptyMessage(MSG_DATA_DISCON);
		}

		@Override
		public void OnGetDeviceVer(int nHWMajor, int nHWMinor, int nSWMajor,
				int nSWMinor) {
			HWMajor = nHWMajor;
			HWMinor = nHWMinor;
			SWMajor = nSWMajor;
			SWMinor = nSWMinor;
		}

		@Override
		public void OnGetSpO2Param(int nSpO2, int nPR, float nPI,
				boolean nStatus, int nMode, float nPower) {
			Message msg = mHandler.obtainMessage(MSG_DATA_SPO2_PARA);
			Bundle data = new Bundle();
			data.putInt("nSpO2", nSpO2);
			data.putInt("nPR", nPR);
			data.putFloat("nPI", nPI);
			data.putFloat("nPower", nPower);
			data.putBoolean("nStatus", nStatus);
			data.putInt("nMode", nMode);
			msg.setData(data);
			mHandler.sendMessage(msg);
		}

		@Override
		public void OnGetSpO2Wave(List<Wave> wave) {
			DRAWDATA.addAll(wave);
			SPOWAVE.addAll(wave);
			if(wave.size()>0) {
				try {
					int[] waveData = new int[wave.size()];
					for(int i=0;i<wave.size();i++)
					{
						waveData[i]=wave.get(i).data;
					}
					Message msg = mHandler.obtainMessage(MSG_DATA_WAVE);
					//Wave waveData = wave.get(0);
					Bundle data = new Bundle();
					//data.putInt("waveData", waveData.data);
					data.putIntArray("waveData",waveData);
					msg.setData(data);
					mHandler.sendMessage(msg);
				}catch(Exception ex)
				{
					String err=ex.getMessage();
				}
			}
		}

	}

}
