package com.creative.model;

import java.util.Date;

public class User extends Entity {
    private String Name;
    private String PWD;
    private int Age;

    public User(int userId, String name, String pwd, int age)
    {
        this.Id = userId;
        this.Name = name;
        this.PWD = pwd;
        this.Age = age;
    }
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPWD() {
        return PWD;
    }

    public void setPWD(String PWD) {
        this.PWD = PWD;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }
}
