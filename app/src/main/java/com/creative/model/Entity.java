package com.creative.model;

import java.util.Date;

public class Entity {
    public int DataRowStatus=0; // 0-unKnow,1-inserted,2-modified,3-deleted
    protected int Id;
    protected Date Created;
    protected Date Updated;
    protected String Ctrparms;
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Date getCreated() {
        return Created;
    }

    public void setCreated(Date createDate) {
        Created = createDate;
    }

    public Date getUpdated() {
        return Updated;
    }

    public void setUpdated(Date updateDate) {
        Updated = updateDate;
    }

    public String getCtrparms() {
        return Ctrparms;
    }

    public void setCtrparms(String ctrparms) {
        Ctrparms = ctrparms;
    }
}
