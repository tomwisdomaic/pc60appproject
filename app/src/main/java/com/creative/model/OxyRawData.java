package com.creative.model;

public class OxyRawData extends Entity  {
    public int getRawValue() {
        return RawValue;
    }

    public void setRawValue(int rawValue) {
        RawValue = rawValue;
    }

    private int RawValue;
}
