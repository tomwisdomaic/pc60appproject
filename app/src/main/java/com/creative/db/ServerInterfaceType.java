package com.creative.db;
import android.content.Context;

import com.cls.InterfaceServiceType;
import com.cls.comClass;
import com.creative.libdemo.R;
import com.creative.recvdata.HttpHelper;
import com.creative.recvdata.RestService;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ServerInterfaceType extends comClass {
    private String URL;
    private InterfaceServiceType interfaceServiceType;
    private HttpHelper httpHelper;

    public RestService getRestService() {
        return restService;
    }

    public void setRestService(RestService restService) {
        this.restService = restService;
    }

    private RestService restService;
    private Map<String, String> ParmValues;
    public ServerInterfaceType(Context context,InterfaceServiceType serviceType) {
        URL = context.getResources().getString(R.string.app_api_resturl);
        ParmValues = new HashMap<String, String>();
        interfaceServiceType = serviceType;
    }
    public ServerInterfaceType(InterfaceServiceType serviceType, String url) {
        URL = url;
        ParmValues = new HashMap<String, String>();
        interfaceServiceType = serviceType;
    }

    public void ClearHttpParms() {
        ParmValues.clear();
    }

    /**
     * save http call parms
     *
     * @param parmName
     * @param parmValue
     */
    public void PushHttpParmsValue(String parmName, String parmValue) {
        if (ParmValues.keySet().contains(parmName)) {
            ParmValues.remove(parmName); //update
        }
        ParmValues.put(parmName, parmValue);
    }

    private String ParmsValue2Json()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            if (ParmValues == null) {
                jsonObject.accumulate("value", "");
            } else {
                for (String key : ParmValues.keySet()) {
                    jsonObject.accumulate(key, ParmValues.get(key));
                }
            }
        }catch(Exception ex)
        {
            error = ex;
        }
        String json = jsonObject.toString();
        return json;
    }
    public boolean PostData() {
        switch (interfaceServiceType) {
            case UrlParmsPost: {
                try {
                    if (httpHelper == null) {
                        httpHelper = new HttpHelper(URL);
                    }
                    httpHelper.setParmValues(ParmValues);
                    httpHelper.PostAll();
                    while (httpHelper.isPosting()) {
                        ;
                    }
                    error = httpHelper.error;
                    return httpHelper.isSuccessPost();
                }catch(Exception ex) {
                    error = ex;
                    return false;
                }
            }
            case Restful:{
                try{
                    if (restService == null) {
                        restService = new RestService(URL);
                    }
                    restService.Post(0,ParmsValue2Json());
                }catch(Exception ex) {
                    error = ex;
                    return false;
                }
            }
        }
        return true;
    }
}
