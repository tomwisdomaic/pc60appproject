package com.creative.db;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.creative.model.User;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DBManager {
    private DatabaseHelper helper = null;
    private SQLiteDatabase db = null;
    private Exception error = null;
    private Context dbContext = null;
    public Exception getError() {
        return error;
    }

    public void setError(Exception error) {
        this.error = error;
    }
    /**
     * Construct
     * @param context
     */
    public DBManager(Context context) {
        this.dbContext = context;
        this.openDatabase();
        initialDatabase();
    }

    public void openDatabase()
    {
        helper = new DatabaseHelper(dbContext,"hrvdb.db",1);
        db = helper.getWritableDatabase();
    }
    private void initialDatabase()
    {
        List<User> users = new ArrayList<User>();
        User user = new User(0,"陈劲","111111",18);
        users.add(user);
        this.addModifyUsers(users);
    }
    /**
     * close db
     */
    public void Close() {
        if (db != null) {
            helper.close();
        }
    }
    /**
     * Remove record from table, the table should have the key field Id
     * @param id
     * @param table
     */
    private void delete(int id, String table) {
        // to make sure db is open;
        if(!db.isOpen()) openDatabase();
        db.execSQL("delete from "+table+" where id='"+id+"'");
    }

    /**
     * Construct retrieve daa sql
     * @param idFrom when <=0 read from beginning
     * @param idTo when zero read to end
     * @return
     */
    private Cursor retrieveTableRecords(int idFrom, int idTo,String TableName)
    {
        Cursor c = null;
        if(idTo<=0)
        {
            c = db.rawQuery(String.format("select * from %1$s where id>=%2$s",TableName,idFrom), null);
        }else
        {
            c = db.rawQuery(String.format("select * from %1$s where id>=%2$s and id<=%3$s" ,TableName,idFrom,idTo), null);
        }
        return c;
    }

    /**
     * add users
     */
    public boolean addModifyUsers(List<User> users) {
        this.setError(null);
        // to make sure db is open;
        if(!db.isOpen()) openDatabase();
        try {
            StringBuffer sbSQL = new StringBuffer();
            db.beginTransaction();
            Date currentTime = Calendar.getInstance().getTime();
            for (int i = 0; i < users.size(); i++) {
                User user = users.get(i);
                int id = user.getId();
                if (id >0 && user.DataRowStatus == 3 ) {
                    //remove the user which is tagged as deleted id<0
                    delete(id, "user");
                }
                if (i != 0) {
                    sbSQL.delete(0, sbSQL.length());
                }
                if(id<=0) {
                    //insert new record
                    sbSQL.append(" INSERT INTO ").append("user").append("(name, pwd, age,created) VALUES");
                    sbSQL.append(" ('").append(user.getName())
                            .append("','").append(user.getPWD())
                            .append("',").append(user.getAge())
                            .append(",").append("DATETIME('now')")
                            //.append(",").append(user.getCtrparms())
                            .append(");");
                }else
                {
                    //update existing record
                    sbSQL.append(" UPDATE ").append("user").append(" set name='").append(user.getName())
                            .append("',pwd='").append(user.getPWD())
                            .append("',age=").append(user.getAge())
                            .append(",updated=").append("DATETIME('now')")
                            //.append(",ctrparms=").append(user.getCtrparms())
                            .append(" where id=").append(id)
                            .append(";");
                }
                db.execSQL(sbSQL.toString());
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            return true;
        }catch(Exception ex)
        {
            this.setError(ex);
            return false;
        }
    }
    public List<User> getAllUser() {
        this.setError(null);
        // to make sure db is open;
        if (!db.isOpen()) openDatabase();
        Cursor cursor = retrieveTableRecords(0, 0,"user");//read all data rows
        List<User> users = new ArrayList<User>();
        try {
            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        int UserId = cursor.getInt(cursor.getColumnIndex("id"));
                        String userName = cursor.getString(cursor.getColumnIndex("name"));
                        String pwd = cursor.getString(cursor.getColumnIndex("pwd"));
                        int Age = cursor.getInt(cursor.getColumnIndex("age"));
                        User user = new User(UserId, userName, pwd, Age);
                        users.add(user);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception ex) {
            this.setError(ex);
        } finally {
            cursor.close();
        }
        return users;
    }

}

