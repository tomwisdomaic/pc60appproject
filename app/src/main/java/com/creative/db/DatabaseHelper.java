package com.creative.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context, String dbname, int version) {
        super(context, dbname, null, version);
        // TODO Auto-generated constructor stub
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        deleteTable(db,"user");
        //getWritableDatabase() or getReadableDatabase() will open the database
        String sql_user="create table if not exists user("
                +"id integer primary key autoincrement,"
                +"name text,"
                +"pwd text,"
                +"age integer,"
                +"created datetime,"
                +"updated datetime,"
                +"ctrparms text)";
        db.execSQL(sql_user);// create user table to save the user
    }
    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion, int newVersion)
    {

    }
    public void deleteTable(SQLiteDatabase db, String TableName) {
        String sql_delteTable = "DROP TABLE if exists " + TableName;
        db.execSQL(sql_delteTable);
    }
    }
