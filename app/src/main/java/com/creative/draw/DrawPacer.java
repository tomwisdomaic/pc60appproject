package com.creative.draw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.creative.base.BaseDate;
import com.creative.libdemo.R;
import com.creative.recvdata.StaticReceive;

public class DrawPacer extends View implements Runnable {
    /**
     * Pacer
     */
    private RectF pacerRect;

    private Paint mPaint = new Paint();

    private DisplayMetrics dm;
    /**
     * zoom rate
     */
    private float scalePacer = 0.0f;

    /** current pacer height value */
    private int pacerValue = 0;

    protected boolean stop = false;
    protected boolean pause = false;

    public DrawPacer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DrawPacer(Context context, AttributeSet attrs,
    int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public DrawPacer(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        WindowManager wmManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        dm = new DisplayMetrics();
        wmManager.getDefaultDisplay().getMetrics(dm);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(dm.density * 3);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        pacerRect = new RectF(0, 0, w, h);
        scalePacer = pacerRect.height() / 127f;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (pacerRect != null) {
            mPaint.setColor(getResources().getColor(R.color.color_main_yellow));
            mPaint.setStyle(Paint.Style.STROKE);
            canvas.drawRect(pacerRect, mPaint);
            DrawPacer(canvas);
        }
    }

    private void DrawPacer(Canvas canvas) {
        mPaint.setColor(Color.rgb(0x03, 0x87, 0x06));
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawRect(pacerRect.left + 5, getPacer(pacerValue) - 5, pacerRect.right - 5,
                pacerRect.bottom - 5, mPaint);
    }

    /**
     * 计算 血氧数据的绘制高度
     */
    private float getPacer(int d) {
        return pacerRect.bottom - scalePacer * d;
    }

    @Override
    public void run() {
        synchronized (this) {
            while (!stop) {
                try {
                    if (pause) {
                        this.wait();
                    }
                    if (StaticReceive.SPOWAVE.size() > 0) {
                        BaseDate.Wave data = StaticReceive.SPOWAVE.remove(0);
                        this.pacerValue = data.data;
                        postInvalidate();
                        if (StaticReceive.SPOWAVE.size() > 25) {
                            Thread.sleep(17);
                        } else {
                            Thread.sleep(20);
                        }
                    } else {
                        Thread.sleep(100);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void Stop() {
        this.stop = true;
    }

    public void Pause() {
        this.pause = true;
    }

    public boolean isPause() {
        return this.pause;
    }

    public boolean isStop() {
        return this.stop;
    }

    public synchronized void Continue() {
        this.pause = false;
        this.notify();
    }
}
