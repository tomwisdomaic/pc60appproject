package com.creative.draw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

public class BackGround extends View {

	public static DisplayMetrics dm;

	/** real mm nunit value for each pixel on X axel*/
	public static float xPX2MMUnit = 0.0f;

	/** real mm nunit value for each pixel on Y axel*/
	public static float yPX2MMUnit = 0.0f;

	/** current view width in mm */
	private float width = 0.0f;

	/** current view height in mm */
	public static float height = 0.0f;

	/** background scale min height in mm */
	protected static final int minHeight = 4;

	/** background scale real height in mm */
	public static float gridHeigh = 0.0f;

	/** total background scale cells count */
	public static int gridCnt = 0;

	/** drawing area width in pixels */
	public static int mWidth = 0;

	/** drawing area height in pixels */
	public static int mHeight = 0;

	private int backgroundColor = 0;

	private Paint mPaint;

	public BackGround(Context context) {
		super(context);
		initScreen(context);
	}

	public BackGround(Context context, AttributeSet attrs) {
		super(context, attrs);
		initScreen(context);
	}

	public BackGround(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initScreen(context);
	}

	private void initScreen(Context context) {
		WindowManager wmManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		dm = new DisplayMetrics();
		wmManager.getDefaultDisplay().getMetrics(dm);
		xPX2MMUnit = 25.4f / dm.densityDpi;
		yPX2MMUnit = 25.4f / dm.densityDpi;
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setStrokeWidth(1);
		backgroundColor = Color.WHITE;
		setBackgroundColor(Color.BLUE);
	}
	/**
	 * background color
	 */
	public void setBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	private boolean isDrawBG = true;

	/**
	 * to draw background
	 * 
	 * @param isDrawBG
	 */
	public void setDrawBG(boolean isDrawBG) {
		this.isDrawBG = isDrawBG;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (!isDrawBG)
			return;
		canvas.drawColor(backgroundColor);
		if (gridCnt < 2) {
			return;
		}
		mPaint.setStrokeWidth(1);
		mPaint.setColor(Color.rgb(0xba, 0xba, 0xba));
		// draw Y axel
		for (float i = 0; i < width; i += gridHeigh) {
			canvas.drawLine(fMMgetPxforX(i), 0, fMMgetPxforX(i), mHeight,
					mPaint);
		}

		int i = gridCnt / 2;
		for (int j = 0; j < i; j++) {
			canvas.drawLine(0, fMMgetPxfory(fPXgetMMforY(mHeight / 2)
					- gridHeigh * j), mWidth,
					fMMgetPxfory(fPXgetMMforY(mHeight / 2) - gridHeigh * j),
					mPaint);
		}

		for (int j = 0; j < i; j++) {
			canvas.drawLine(0, fMMgetPxfory(fPXgetMMforY(mHeight / 2)
					+ gridHeigh * j), mWidth,
					fMMgetPxfory(fPXgetMMforY(mHeight / 2) + gridHeigh * j),
					mPaint);
		}
		drawScale(canvas);
	}

	/**
	 * to draw Scale
	 */
	private boolean isDrawScale = false;

	/**
	 * set draw Scale
	 */
	public void setDrawScale(boolean isDrawScale) {
		this.isDrawScale = isDrawScale;
	}

	/** drawing scale */
	private void drawScale(Canvas canvas) {
		if (gridHeigh > 1 && isDrawScale) {
			int h = mHeight / gridCnt;// one cell height
			mPaint.setColor(Color.BLUE);
			mPaint.setStrokeWidth(dm.density);
			float i = (h * gain) / 2f;
			canvas.drawLine(0, mHeight / 2 - i, h / 2, mHeight / 2 - i, mPaint);
			canvas.drawLine(0, mHeight / 2 + i, h / 2, mHeight / 2 + i, mPaint);
			canvas.drawLine(h / 4, mHeight / 2 - i, h / 4, mHeight / 2 + i,
					mPaint);
		}
	}

	private float gain = 2;

	public float getGain() {
		return gain;
	}

	public void setGain(float gain) {
		if (gain == 0) {
			this.gain = 0.5f;
		} else
			this.gain = gain;
		postInvalidate();
	}

	/**
	 *
	 * get distance in pixels on X by the mm value
	 * @param mm
	 * @return
	 */
	public static float fMMgetPxforX(float mm) {
		return mm / xPX2MMUnit;
	}

	/**
	 *
	 * convert pixels to mm length on X axel
	 * @param px
	 * @return
	 */
	public static float fPXgetMMforX(int px) {
		return px * xPX2MMUnit;
	}

	/**
	 *
	 * convert mm to pixels length on Y axel
	 * @param mm
	 * @return
	 */
	public static float fMMgetPxfory(float mm) {
		return mm / yPX2MMUnit;
	}

	/**
	 *
	 * convert pixels to mm length on Y axel
	 * @param px
	 * @return
	 */
	public static float fPXgetMMforY(int px) {
		return px * yPX2MMUnit;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		setViewHeight(w, h);
	}

	public void setViewHeight(int w, int h) {
		// Convert screen pixels to mm
		this.width = fPXgetMMforX(w);
		height = fPXgetMMforY(h);
		mHeight = h;
		mWidth = w;

		gridCnt = 6;
		gridHeigh = (float) (height / gridCnt);
		postInvalidate();
	}

	/** total grid count */
	public int getGridCnt() {
		return gridCnt;
	}

	/** set total grid count */
	public void setGridCnt(int gridCnt) {
		BackGround.gridCnt = gridCnt;
	}

	/** get real scale height (mm) */
	public float getGridHeigh() {
		return gridHeigh;
	}

	/** set real scale height (mm) */
	public void setGridHeigh(float gridHeigh) {
		BackGround.gridHeigh = gridHeigh;
	}

}
