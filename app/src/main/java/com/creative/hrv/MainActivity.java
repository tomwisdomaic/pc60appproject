package com.creative.hrv;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.cls.InterfaceServiceType;
import com.cls.comFun;
import com.creative.base.BLUReader;
import com.creative.base.BLUSender;
import com.creative.bluetooth.BluetoothOpertion;
import com.creative.bluetooth.IBluetoothCallBack;
import com.creative.db.ServerInterfaceType;
import com.creative.draw.DrawPC300SPO2Rect;
import com.creative.draw.DrawThreadNW;
import com.creative.libdemo.R;
import com.creative.recvdata.RestService;
import com.creative.recvdata.StaticReceive;
public class MainActivity extends comActivity {

	private TextView tvMsg;
	private TextView tvWaveData;
	private RestService restService;

	private DrawThreadNW drawWave;

	private Thread drawThread;

	/**
	 * 300 clinder ,draw spo2 rect runnable
	 */
	private DrawPC300SPO2Rect drawPC300SPO2Rect;

	/** Draw 300 clinder thread  ,  draw spo2 rect thread */
	private Thread drawPC300SPO2RectThread;

	private String TAG ="BloodOxygen";
	private BluetoothOpertion mBluetoth;
	private BluetoothSocket mSocket;
	private ListView listViewSeach;
	private ListView listViewBond;
	private SimpleAdapter seachListAdapter;
	private List<Map<String, String>> seachDeviceListString = new ArrayList<Map<String, String>>();
	private List<BluetoothDevice> bondDeviceList = new ArrayList<BluetoothDevice>();
	private List<BluetoothDevice> seachDeviceList = new ArrayList<BluetoothDevice>();

	private BluetoothDevice device = null; // current BluetoothDevice
	private Date dataStartPoint = Calendar.getInstance().getTime();
	private String RawDataString_Cache=""; //raw data from sensor
	private Integer RawDataString_Cache_Cnt=0;

	private boolean startGetData=true;

	private long _testTime= 300000; //1mins = 60000ms
    private long startTime = 0;
    private TextView timerTextView;

    Handler timerHandler;
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            long millis = System.currentTimeMillis() - startTime;
			if(_testTime+1000<millis)
			{
				_testTime = 0;
				startGetData=false;
			}
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;
            timerTextView.setText(String.format("%d:%02d", minutes, seconds));
            timerHandler.postDelayed(this, 500);
        }
    };
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB_MR2){
			TAG = getResources().getString(R.string.app_version_notsupport);;
		}else {
			TAG = getResources().getString(R.string.app_main_title);
		}
		setContentView(R.layout.activity_main);
        try {
            mBluetoth = new BluetoothOpertion(this, new BluetothCallBack());
            findViewById(R.id.btnBreakConnection).setOnClickListener(MyListener);
            findViewById(R.id.btnUserManage).setOnClickListener((MyListener));
            findViewById(R.id.btnExitAPP).setOnClickListener(MyListener);
            findViewById(R.id.timerTextView).setOnClickListener(MyListener);
            tvMsg = (TextView) findViewById(R.id.para);
            timerTextView =(TextView)findViewById(R.id.timerTextView);
			startTime = System.currentTimeMillis();
            drawWave = (DrawThreadNW) findViewById(R.id.drawPC300);
			drawPC300SPO2Rect = (DrawPC300SPO2Rect) findViewById(R.id.realplay_draw_spo_rect);
			tvWaveData =(TextView) findViewById(R.id.original_WaveValue);
            StaticReceive.setmHandler(myHandler);
            seachListAdapter = new SimpleAdapter(this, seachDeviceListString,
                    android.R.layout.simple_list_item_1, new String[]{"name"},
                    new int[]{android.R.id.text1});
			OpenAndConnectBluetooth();
        }catch(Exception ex)
        {
            error = ex;
        }
	}
	private void OpenAndConnectBluetooth()
	{
		if(!mBluetoth.isOpen())
		{
			mBluetoth.open();
		}
		if(mSocket==null)
		{
			mBluetoth.discovery();
			showListDialog();
		}
	}
	private OnClickListener MyListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.btnBreakConnection) {// break bluetooth
				Button btnBreakConnection = (Button)v;
				if (mSocket != null) {
					mBluetoth.disConnect(mSocket);

					btnBreakConnection.setText(getResources().getString(R.string.app_main_connection_reconnection));
				}else if(device!=null  && btnBreakConnection.getText()==getResources().getString(R.string.app_main_connection_reconnection))
				{
					mBluetoth.connect(device);
					btnBreakConnection.setText(getResources().getString(R.string.app_main_connection_break));
				}
			}else if(v.getId() == R.id.btnUserManage) {// user manage
				try {
					Intent intent = new Intent();
					intent.setClass(MainActivity.this, UserActivity.class);
					intent.putExtra("currentUserId", "ChenJing");
					startActivity(intent);
				}catch(Exception ex)
				{
					String err =ex.getMessage();
					tvMsg.setText(err);
				}
            }else if(v.getId()==R.id.btnExitAPP)
			{
                //closeFile();
				if(RawDataString_Cache_Cnt>0) {
					try {
						saveData(RawDataString_Cache);
					} catch (Exception ex) {
						error = ex;
					}
				}
				agentApplication.Exit();
			}else if(v.getId()==R.id.timerTextView)
			{
				TextView tv = (TextView) findViewById(R.id.timerTextView);
				if(String.valueOf(tv.getText()).equals( "0:00")) {
					startTime = System.currentTimeMillis();
					if (timerHandler == null) {
						timerHandler = new Handler();
						timerRunnable.run();
					} else {
						timerHandler.postDelayed(timerRunnable, 0);
					}
					startGetData = true;
					//rest the timers0:00
					_testTime = 300000;
					RawDataString_Cache = "";
					RawDataString_Cache_Cnt = 0;
				}else{
					timerHandler.removeCallbacks(timerRunnable);
					startGetData = false;
					tv.setText("0:00");
					if(_testTime<=0 && RawDataString_Cache_Cnt>0) {
						try {
							saveData(RawDataString_Cache);
						} catch (Exception ex) {
							error = ex;
						}
					}
				}
			}
		}
	};

	private Dialog listDialog;

	private void showListDialog() {
		tvMsg.setText(getResources().getString(R.string.app_main_device_onsearch));
		if (listDialog == null) {
			listDialog = new Dialog(this);
			listDialog.setTitle(getResources().getString(R.string.app_main_device_choose));
			View view = getLayoutInflater().inflate(R.layout.devicelist, null);
			listViewSeach = (ListView) view.findViewById(R.id.list_seach);
			listViewBond = (ListView) view.findViewById(R.id.list_bond);
			listViewBond.setOnItemClickListener(itemClickListener);
			listViewSeach.setOnItemClickListener(itemClickListener);
			listDialog.setContentView(view);
		}
		seachDeviceListString.clear();
		seachDeviceList.clear();

		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		bondDeviceList.clear();
		Set<BluetoothDevice> devices = mBluetoth.getBondedDevices();
		for (Iterator<BluetoothDevice> iterator = devices.iterator(); iterator
				.hasNext();) {
			BluetoothDevice bluetoothDevice = (BluetoothDevice) iterator.next();
			Map<String, String> map = new ArrayMap<String, String>();
			map.put("name",
					bluetoothDevice.getName() + "\n"
							+ bluetoothDevice.getAddress());
			list.add(map);
			bondDeviceList.add(bluetoothDevice);
		}

		SimpleAdapter adapter = new SimpleAdapter(this, list,
				android.R.layout.simple_list_item_1, new String[] { "name" },
				new int[] { android.R.id.text1 });
		listViewBond.setAdapter(adapter);
		listViewSeach.setAdapter(seachListAdapter);
		listDialog.show();

	}

    /** choose the bluetooth matched
     *
     */
	private OnItemClickListener itemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			tvMsg.setText(getResources().getString(R.string.app_main_connection_onconnect));
			listDialog.dismiss();
			if (parent.getId() == listViewBond.getId()) {//bind device id
				device = bondDeviceList.get(position);
			} else {// searching device
				device = seachDeviceList.get(position);
			}
			mBluetoth.connect(device);
		}
	};

	private void saveData(String dataStr)
	{
		try {
			ServerInterfaceType svt = new ServerInterfaceType(MainActivity.this,InterfaceServiceType.Restful);
			//svt.PushHttpParmsValue("data",RawDataString_Cache);
			//boolean result = svt.HttpPostData();
			String startPoint = comFun.GetDateTimeDBString(dataStartPoint);
            svt.PushHttpParmsValue("id","0");
			svt.PushHttpParmsValue("userid","1001");
            svt.PushHttpParmsValue("created",startPoint);
            svt.PushHttpParmsValue("value",RawDataString_Cache);
            svt.PostData();
			RawDataString_Cache = "";
			RawDataString_Cache_Cnt=0;
		}catch(Exception ex)
		{
			error = ex;
			String err =ex.getMessage();
		}
	}

	private Handler myHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case 0: {
				tvMsg.setText((String) msg.obj);
			}
				break;
			case StaticReceive.MSG_DATA_DISCON: {//break connection to device
				mBluetoth.disConnect(mSocket);
				mSocket = null;
				tvMsg.setText(getResources().getString(R.string.app_main_device_break));
				drawWave.cleanWaveData();
			}
				break;
			case StaticReceive.MSG_DATA_SPO2_PARA: {//show parameter
				Bundle data = msg.getData();
				if (!data.getBoolean("nStatus")) {// detector slides off
					tvMsg.setText(getResources().getString(R.string.app_main_device_detectoroff));
					drawWave.cleanWaveData();
				} else {
					try {
					String para = getResources().getString(R.string.app_name_object_SPO2)+"=" + data.getInt("nSpO2") + "%  "+
							getResources().getString(R.string.app_name_object_PR)+"="
							+ data.getInt("nPR") + "bmp  PI="
							+ (data.getFloat("nPI"));
						tvMsg.setText(para);
					}catch(Exception ex)
					{
						error = ex;
					}
				}
			}
				break;
			case StaticReceive.MSG_DATA_PULSE: {// there are pulse show tag of pulse
				Log.v(TAG, getResources().getString(R.string.app_main_tag_pulse));
			}
				break;

			case StaticReceive.MSG_DATA_WAVE:{
				Bundle data = msg.getData();
				int[] v = data.getIntArray("waveData");
				if(startGetData) {
					for(int i=0;i<v.length;i++) {
						if (RawDataString_Cache == "") {
							dataStartPoint = Calendar.getInstance().getTime();
							RawDataString_Cache = String.valueOf(v[i]);
						} else {
							RawDataString_Cache = RawDataString_Cache + "," + String.valueOf(v[i]);
						}
					}
					RawDataString_Cache_Cnt ++;
				}
				tvWaveData.setText(String.valueOf(v[0]) +"/" +String.valueOf(RawDataString_Cache_Cnt));
			}
			break;
			}
		}
	};

    @Override
	protected void onDestroy() {
		if (mSocket != null) {
			StaticReceive.StopReceive();
			mBluetoth.disConnect(mSocket);
		}
		if (drawWave != null) {
			drawWave.Stop();
			drawWave = null;
		}
		if (drawThread != null) {
			drawThread = null;
		}

		if(drawPC300SPO2Rect!=null)
		{
			drawPC300SPO2Rect.Stop();
			drawPC300SPO2Rect = null;
		}
		if(drawPC300SPO2RectThread!=null)
		{
			drawPC300SPO2RectThread=null;
		}
		super.onDestroy();
	};

	private class BluetothCallBack implements IBluetoothCallBack {

		@Override
		public void OnConnectFail(String arg0) {
			myHandler.obtainMessage(0, getResources().getString(R.string.app_main_connection_failed));
		}

		@Override
		public void OnConnected(BluetoothSocket arg0) {
			myHandler.obtainMessage(0, getResources().getString(R.string.app_main_connection_success));
			mSocket = arg0;
			try {
				StaticReceive.startReceive(MainActivity.this, arg0
						.getRemoteDevice().getName(),
						new BLUReader(arg0.getInputStream()), new BLUSender(
								arg0.getOutputStream()), myHandler);
				drawWave.setmHandler(myHandler);
				drawThread = new Thread(drawWave, "DrawPC60NW");
				drawThread.start();
				/** at the same time start the clinder thread
				 * *
				 */
				drawPC300SPO2RectThread = new Thread(drawPC300SPO2Rect, "DrawPC300RectThread");
				drawPC300SPO2RectThread.start();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void OnDiscoveryCompleted(List<BluetoothDevice> arg0) {

		}

		@Override
		public void OnException(int arg0) {

		}

		@Override
		public void OnFindDevice(BluetoothDevice arg0) {
			if (seachDeviceList.contains(arg0))
				return;
			Map<String, String> map = new ArrayMap<String, String>();
			map.put("name", arg0.getName() + "\n" + arg0.getAddress() + "\n");
			seachDeviceListString.add(map);
			seachDeviceList.add(arg0);
			seachListAdapter.notifyDataSetChanged();
		}

	}
}
