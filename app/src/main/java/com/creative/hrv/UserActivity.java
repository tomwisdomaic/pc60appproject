package com.creative.hrv;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.creative.db.DBManager;
import com.creative.libdemo.R;
import com.creative.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class UserActivity extends comActivity{

    private List<User> userList = new ArrayList<User>();
    private ListView userListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        String keyStr = bundle.getString("currentUserId");
        TextView currentuser=(TextView)findViewById(R.id.tv_currentuser);
        currentuser.setText(keyStr);
        deleteUsers();
        showUsers();
    }
    private void showUsers()
    {
        try {
            List<Map<String, String>> list = new ArrayList<Map<String, String>>();
            //get users in the sqlite and map to list
            DBManager dbManager = new DBManager(this);
            userList = dbManager.getAllUser();
            for(int i=0;i<userList.size();i++){
                Map<String,String> item = new HashMap<String, String>();
                item.put("name",userList.get(i).getName());
                list.add(item);
            }
            userListView = (ListView) findViewById(R.id.list_user);
            SimpleAdapter adapter = new SimpleAdapter(this, list,
                    android.R.layout.simple_list_item_1, new String[]{"name"},
                    new int[]{android.R.id.text1});
            userListView.setAdapter(adapter);
        }catch(Exception ex)
        {
            this.error = ex;
        }
    }
    private void deleteUsers()
    {
        DBManager dbManager = new DBManager(this);
        userList = dbManager.getAllUser();
        for(int i=0;i< userList.size();i++)
        {
            userList.get(i).DataRowStatus = 3; //tagged as to be removed
        }
        dbManager.addModifyUsers(userList);
    }
}
