package com.creative.hrv;

import android.os.Bundle;
import android.app.Activity;

import com.creative.recvdata.AgentApplication;

/**
 * transfer parent class Activity for all the inherit
 */
public class comActivity extends Activity {
    protected Exception error;
    protected AgentApplication agentApplication;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            agentApplication = new AgentApplication();
            agentApplication.setApplication(this.getApplication());
            agentApplication.AddActivity(this);
        }catch(Exception ex) {
            error=ex;
        }

    }
}
