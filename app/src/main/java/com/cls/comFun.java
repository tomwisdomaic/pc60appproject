package com.cls;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class comFun {
    /**
     * all the datetime format for datebase
     * @param date
     * @return
     */
    public static String GetDateTimeDBString(Date date)
    {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }
}
